# Partie 1

## Sous-partie 1 : texte

Une phrase sans rien

_Une phrase en italique_

**Une phrase en gras**

Un lien vers [fun-mooc.fr](https://www.fun-mooc.fr/fr/)

Une ligne de `code`

## Sous-partie 2 : listes

### Liste à puces

* item
 * sous-item
 * sous-item

### Liste numérotées

1. item
2. item
3. item

## Sous-partie 3 : code

```python
# Extrait de code
```
