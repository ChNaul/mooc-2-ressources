Objectifs : Utilisation du module Tutle et familiarisation avec le langage Python
Pré-requis à cette activité : langage Python, boucle for, while et conditionnelle
Durée de l'activité : 4h
Exercices cibles : 
Description du déroulement de l'activité : Travail individuel ou en binôme
Anticipation des difficultés des élèves : utilisation des boucles et appropritation langage turtle
Gestion de l'hétérogénéïté : fiche d'aide
